/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.vnfmanager.tools.dsl.generator


import org.apache.commons.io.FileUtils
import org.onap.ccsdk.cds.controllerblueprints.core.data.DataType
import org.onap.ccsdk.cds.controllerblueprints.core.data.NodeType
import org.onap.ccsdk.cds.controllerblueprints.core.dsl.dataType
import org.onap.ccsdk.cds.controllerblueprints.core.dsl.nodeType
import org.onap.ccsdk.cds.controllerblueprints.core.jsonAsJsonType
import org.onap.ccsdk.cds.controllerblueprints.core.normalizedFile
import org.onap.ccsdk.cds.controllerblueprints.core.utils.BluePrintMetadataUtils
import org.onap.ccsdk.cds.controllerblueprints.core.utils.JacksonUtils
import kotlin.test.Test

class DSLServiceTemplateGeneratorTest {


    @Test
    fun testDataType() {

        val blueprintContext = BluePrintMetadataUtils
                .getBluePrintContext("/Users/brindasanth/brvith/vnf-manager/components/model-catalog/blueprint-model/test-blueprint/baseconfiguration")

        val dsl = blueprintContext.serviceTemplate.dsl()

        FileUtils.write(normalizedFile("src/test/kotlin", "generated.kt"), dsl)

    }

    @Test
    fun testNodeType() {
        val nodeType = JacksonUtils
                .readValueFromFile("/Users/brindasanth/brvith/vnf-manager/components/model-catalog/definition-type/starter-type/node_type/component-restconf-executor.json"
                        , NodeType::class.java)!!

        println(nodeType.dsl("source-rest"))
    }
}