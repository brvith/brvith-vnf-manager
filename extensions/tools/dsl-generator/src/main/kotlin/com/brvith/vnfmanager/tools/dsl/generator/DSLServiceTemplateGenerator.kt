/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.vnfmanager.tools.dsl.generator

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.*
import org.jetbrains.kotlin.serialization.js.DynamicTypeDeserializer.id
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintConstants
import org.onap.ccsdk.cds.controllerblueprints.core.BluePrintException
import org.onap.ccsdk.cds.controllerblueprints.core.asJsonString
import org.onap.ccsdk.cds.controllerblueprints.core.data.*
import org.onap.ccsdk.cds.controllerblueprints.core.dsl.artifactType
import org.onap.ccsdk.cds.controllerblueprints.core.dsl.dataType
import org.onap.ccsdk.cds.controllerblueprints.core.dsl.nodeType
import org.onap.ccsdk.cds.controllerblueprints.core.dsl.relationshipType
import org.onap.ccsdk.cds.controllerblueprints.core.format
import org.onap.ccsdk.cds.controllerblueprints.core.service.BluePrintExpressionService


fun ServiceTemplate.dsl(): String {
    val content: StringBuilder = StringBuilder()
    content.append("import org.onap.ccsdk.cds.controllerblueprints.core.dsl.*")
    content.append("\n\nval serviceTemplate = ")

    val metadata = this.metadata!!

    val name = metadata[BluePrintConstants.METADATA_TEMPLATE_NAME]!!
    val version = metadata[BluePrintConstants.METADATA_TEMPLATE_VERSION]
    val author = metadata[BluePrintConstants.METADATA_TEMPLATE_AUTHOR]
    val tags = metadata[BluePrintConstants.METADATA_TEMPLATE_TAGS]

    content.append("""
        serviceTemplate(name = "$name",
                version = "${version}",
                author = "${author}",
                tags = "${tags}") {""")

    content.append("\n")
    content.append(metaData(metadata))
    content.append("\n\ntopologyTemplate {")
    content.append("\n")
    content.append(this.topologyTemplate?.dsl())
    content.append("}")


    this.dataTypes?.let { content.append(dataTypesDSL(it)) }
    this.artifactTypes?.let { content.append(artifactTypesDSL(it)) }
    this.nodeTypes?.let { content.append(nodeTypesDSL(it)) }
    this.relationshipTypes?.let { content.append(relationshipTypesDSL(it)) }

    content.append("\n}")
    return content.toString()
}

fun TopologyTemplate.dsl(): String {
    val content: StringBuilder = StringBuilder()
    this.nodeTemplates?.let { content.append(nodeTemplatesDSL(it)) }
    return content.toString()
}

fun nodeTemplatesDSL(nodeTemplates: Map<String, NodeTemplate>): String {
    val content: StringBuilder = StringBuilder()
    content.append("\n\n")
    nodeTemplates.forEach { name, nodeTemplate ->
        content.append("\n")
        content.append(nodeTemplate.dsl(name))
    }
    return content.toString()
}

fun NodeTemplate.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    content.append("""
        nodeTemplate(id = "$name",
                type = "${this.type}",
                description = "${this.description}") {""")
//    this.properties?.forEach { propName, prop ->
//        content.append("""
//                ${prop.dsl(propName)}""")
//    }
    this.interfaces?.forEach { intfName, intf ->
        content.append("""
                ${intf.dsl(intfName)}""")
    }
    this.artifacts?.forEach { artifactName, artifact ->
        content.append("\n")
        content.append(artifact.dsl(artifactName))
    }
    content.append("""
        }
        """.trimIndent())
    return content.toString()
}

fun ArtifactDefinition.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    content.append("""artifact( id = "${name}", type ="${this.type}", file = "${this.file}") """)
    return content.toString()
}

fun InterfaceAssignment.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    val firstOperationName = this.operations!!.keys.first()
    val firstOperation = this.operations!!.values.first()
    content.append("""operation( interfaceName = "${firstOperationName}", description = "${firstOperation.description}") { """)
    /** Operation Inputs */
    content.append("\ninputs { ")
    firstOperation.inputs?.forEach { propName, prop ->
        content.append("""
                ${assignmentDsl(propName, prop)}""")
    }
    content.append("}")

    /** Operation Outputs */
    content.append("\noutputs { ")
    firstOperation.outputs?.forEach { propName, prop ->
        content.append("""
                ${assignmentDsl(propName, prop)}""")
    }
    content.append("}")

    content.append("""
        }
        """.trimIndent())
    return content.toString()
}

fun assignmentDsl(name: String, assignment: JsonNode): String {
    val content: StringBuilder = StringBuilder("property(id = \"$name\", value= ")
    val expressionData = BluePrintExpressionService.getExpressionData(assignment)
    if (expressionData.isExpression) {
        val command = expressionData.command!!
        when (command) {
            BluePrintConstants.EXPRESSION_GET_INPUT -> {
                content.append("""getInput("${expressionData.inputExpression?.propertyName!!}")""")
            }
            BluePrintConstants.EXPRESSION_GET_ATTRIBUTE -> {
                val attributeExpression = expressionData.attributeExpression!!
                if (attributeExpression.subAttributeName != null) {
                    content.append("""getAttribute("${attributeExpression.attributeName}", "${attributeExpression.subAttributeName}")""")
                } else {
                    content.append("""getAttribute("${attributeExpression.attributeName}")""")
                }
            }
            BluePrintConstants.EXPRESSION_GET_PROPERTY -> {
                val propertyExpression = expressionData.propertyExpression!!
                if (propertyExpression.subPropertyName != null) {
                    content.append("""getProperty("${propertyExpression.propertyName}", "${propertyExpression.subPropertyName}")""")
                } else {
                    content.append("""getProperty("${propertyExpression.propertyName}")""")
                }
            }
            BluePrintConstants.EXPRESSION_GET_OPERATION_OUTPUT -> {

            }
            BluePrintConstants.EXPRESSION_GET_ARTIFACT -> {
                val artifactExpression = expressionData.artifactExpression!!
                content.append("""getArtifact("${artifactExpression.artifactName}")""")
            }
            BluePrintConstants.EXPRESSION_DSL_REFERENCE -> {
                content.append(""""*${expressionData.dslExpression?.propertyName}"""")
            }
            BluePrintConstants.EXPRESSION_GET_NODE_OF_TYPE -> {

            }
            else -> {
                //throw BluePrintException(format("for property ({}), command ({}) is not supported ", name, command))
            }
        }
    } else {
        content.append(assignment.dsl())
    }
    content.append(")")
    return content.toString()
}

fun JsonNode.dsl(): String {
    val content: StringBuilder = StringBuilder()
    when (this) {
        is TextNode ->
            content.append(""""${this.asText()}"""")
        is BooleanNode ->
            content.append("""${this.asBoolean()} """)
        is IntNode ->
            content.append("""${this.asInt()} """)
        is ObjectNode ->
            content.append("""${this.asJsonString(false).threeQuotes()}""")
        is ArrayNode ->
            content.append("""${this.asJsonString(false).threeQuotes()}""")
        else ->
            content.append("")
    }
    return content.toString()
}

fun String.threeQuotes(): String {
    return "\"\"\"$this\"\"\""
}


fun metaData(metadata: Map<String, String>): String {
    val content: StringBuilder = StringBuilder()
    metadata.forEach {
        content.append("\n")
        content.append("""metadata(id = "${it.key}", value = "${it.value}")""")
    }
    return content.toString()
}

fun dataTypesDSL(dataTypes: Map<String, DataType>): String {
    val content: StringBuilder = StringBuilder()

    content.append("\n\n")
    dataTypes.forEach { name, dataType ->
        content.append("\n")
        content.append(dataType.dsl(name))
    }

    return content.toString()
}

fun artifactTypesDSL(artifactTypes: Map<String, ArtifactType>): String {
    val content: StringBuilder = StringBuilder()
    content.append("\n\n")
    artifactTypes.forEach { name, artifactTypes ->
        content.append("\n")
        content.append(artifactTypes.dsl(name))
    }
    return content.toString()
}

fun nodeTypesDSL(nodeTypes: Map<String, NodeType>): String {
    val content: StringBuilder = StringBuilder()
    content.append("\n\n")
    nodeTypes.forEach { name, nodeType ->
        content.append("\n")
        content.append(nodeType.dsl(name))
    }
    return content.toString()
}

fun relationshipTypesDSL(relationshipTypes: Map<String, RelationshipType>): String {
    val content: StringBuilder = StringBuilder()
    content.append("\n\n")
    relationshipTypes.forEach { name, relationshipType ->
        content.append("\n")
        content.append(relationshipType.dsl(name))
    }
    return content.toString()
}


fun DataType.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    content.append("""
        dataType(id = "$name",
                version = "${this.version}",
                derivedFrom = "${this.derivedFrom}",
                description = "${this.description}") {""")
    this.properties?.forEach { propName, prop ->
        content.append("""
                ${prop.dsl(propName)}""")
    }
    content.append("""
        }
        """.trimIndent())
    return content.toString()
}

fun ArtifactType.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    content.append("""
        artifactType(id = "$name",
                version = "${this.version}",
                derivedFrom = "${this.derivedFrom}",
                description = "${this.description}") {""")
    this.properties?.forEach { propName, prop ->
        content.append("""
                ${prop.dsl(propName)}""")
    }
    content.append("""
        }
        """.trimIndent())
    return content.toString()
}

fun RelationshipType.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    content.append("""
        relationshipType (id = "$name",
                version = "${this.version}",
                derivedFrom = "${this.derivedFrom}",
                description = "${this.description}") {""")
    this.properties?.forEach { propName, prop ->
        content.append("""
                ${prop.dsl(propName)}""")
    }
    content.append("""
        }
        """.trimIndent())
    return content.toString()
}


fun NodeType.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    content.append("""
        nodeType(id = "$name",
                version = "${this.version}",
                derivedFrom = "${this.derivedFrom}",
                description = "${this.description}") {""")
    this.properties?.forEach { propName, prop ->
        content.append("""
                ${prop.dsl(propName)}""")
    }
    this.attributes?.forEach { atttibuteName, attribute ->
        content.append("""
                ${attribute.dsl(atttibuteName)}""")
    }
    this.interfaces?.forEach { intfName, intf ->
        content.append("""
                ${intf.dsl(intfName)}""")
    }
    content.append("""
        }
        """.trimIndent())
    return content.toString()
}

fun InterfaceDefinition.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    val firstOperationName = this.operations!!.keys.first()
    val firstOperation = this.operations!!.values.first()
    content.append("""operation( interfaceName = "${firstOperationName}", description = "${firstOperation.description}") { """)
    /** Operation Inputs */
    content.append("\ninputs { ")
    firstOperation.inputs?.forEach { propName, prop ->
        content.append("""
                ${prop.dsl(propName)}""")
    }
    content.append("}")

    /** Operation Outputs */
    content.append("\noutputs { ")
    firstOperation.outputs?.forEach { propName, prop ->
        content.append("""
                ${prop.dsl(propName)}""")
    }
    content.append("}")

    content.append("""
        }
        """.trimIndent())
    return content.toString()
}


fun EntrySchema.dsl(): String {
    return """entrySchema(entrySchemaType = "${this.type}")"""
}

fun constrains(constraintClasses: List<ConstraintClause>): String {
    val content: StringBuilder = StringBuilder("constrains {")
    constraintClasses.forEach {
        if (it.validValues != null && it.validValues!!.isNotEmpty()) {
            // content.append("validValues( \"\"\" ${it.validValues!!.asJsonString(true)} \"\"\" )")
        }
    }
    content.append("}")
    return content.toString()
}

fun defaultValues(validValues: JsonNode): String {
    return validValues.dsl()
}

fun PropertyDefinition.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    if (this.entrySchema != null || this.defaultValue != null || this.constraints != null) {
        content.append("""property(id = "${name}", type = "${this.type}", required = ${this.required}, description = "${this.description}"){""")
        if (this.entrySchema != null) {
            content.append(""" ${this.entrySchema!!.dsl()} """)
        }
        if (this.defaultValue != null) {
            content.append("\n")
            content.append("""defaultValue(${defaultValues(this.defaultValue!!)}) """)
        }
        if (this.constraints != null) {
            content.append("\n")
            content.append("""${constrains(this.constraints!!)} """)
        }

        content.append(""" }""")
    } else {
        content.append(""" property(id = "${name}", type = "${this.type}", required = ${this.required}, description = "${this.description}")""")
    }
    return content.toString()
}


fun AttributeDefinition.dsl(name: String): String {
    val content: StringBuilder = StringBuilder()
    if (this.entrySchema != null || this.defaultValue != null || this.constraints != null) {
        content.append("""attribute(id = "${name}", type = "${this.type}", required = ${this.required}, description = "${this.description}"){""")
        if (this.entrySchema != null) {
            content.append(""" ${this.entrySchema!!.dsl()} """)
        }
        if (this.defaultValue != null) {
            content.append("\n")
            content.append("""defaultValue(${defaultValues(this.defaultValue!!)}) """)
        }
        if (this.constraints != null) {
            content.append("\n")
            content.append("""${constrains(this.constraints!!)} """)
        }

        content.append(""" }""")
    } else {
        content.append(""" attribute(id = "${name}", type = "${this.type}", required = ${this.required}, description = "${this.description}")""")
    }
    return content.toString()
}

